#include<iostream>
using namespace std;
int main()
{
    int num[5];  //定义一个有 5 个元素的数组
    int n;
    int number = 0; //记录数位
    cin >> n;  //获取输入的数
    for (int i = 0; i < 5; i++) {  //
        if (n == 0) break;  //如果输入的数为 0 则退出这个循环
        else {
            num[i] = n % 10;  //获取最后一位数，num[0] 为最后一个数，num[1] 是倒数第二个数
            n = n / 10;  //n 向前进 1 位,无小数点
            number++;
        }
    }
    cout << number << endl;  //输出数位
    for (int i = number - 1; i >= 0; i--) {
        if (i != 0) {  //如果 num[i] 不是最后一个数，则加一个空格
            cout << num[i] << " ";
        }
        else cout << num[i];  //输出最后一个数
    }
    cout << endl;  //换行
    for (int i = 0; i < number; i++) {
        cout << num[i];  //按照 num[i] 顺序输出
    }
    return 0;
}
