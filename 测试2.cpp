//该文件有个问题，我交换的是数组中的年龄，而其他相关变量却一起和年龄进行了交换

#include<iostream>
using namespace std;

struct hero
{
	string name;
	int age;
	string gender;//性别的英文
};

void paixu(hero Harr[],int len)
{
	for (int i = 0; i < len - 1; i++)
	{
		for (int j = 0; j < len - i - 1; j++)
		{
			if (Harr[j].age < Harr[j+1].age)
			{
				int temp = Harr[j + 1].age;
				Harr[j + 1].age = Harr[j].age;
				Harr[j].age = temp;
			}
		}
	}
}


int main()
{
	struct hero Harr[5] =
	{
		{"刘备 ",23 ,"男"},
		{"关羽 ",22 ,"男"},
		{"张飞 ",20 ,"男"},
		{"赵云 ",21 ,"男"},
		{"貂蝉 ",19 ,"女"}
	};

	int len = sizeof(Harr) / sizeof(Harr[0]);

	paixu(Harr, len);
	for (int i = 0; i < len; i++)
	{
		cout << "英雄的名字：" << Harr[i].name
			<< "年龄：" << Harr[i].age
			<< " 性别 " << Harr[i].gender << endl;
	}
	
	return 0;

}

